const isLogged = (req, res, next) => {
    if (!req.session.user) {
        return res.status(403).redirect("/login");
    }
    next();
};

module.exports = isLogged;