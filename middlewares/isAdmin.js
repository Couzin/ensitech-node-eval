const isAdmin = (req, res, next) => {
    if (req.session.user.access === "admin") {
        next();
    } else {
        return res.status(401).redirect("/");
    }
};

module.exports = isAdmin;