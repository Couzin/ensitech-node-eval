const is18 = (req, res, next) => {
  try {
    const userBirth = new Date(req.body.birthdate).getFullYear();
    console.log(userBirth);
    const currentYear = new Date().getFullYear();
    console.log(currentYear);
    const userAge = currentYear - userBirth;
    console.log(userAge);
    if (userAge < 18) {
      return res.status(403).json({ message: "You are not allowed to register" });
    }
    next();
  } catch (error) {
    console.log(error);
    return res.status(500).json(error);
  }
};

module.exports = is18;