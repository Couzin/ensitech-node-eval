const getCartQuantity = (req, res, next) => {
    let cartQuantity = 0;
    if (req.session.cart) {   
        for (let i = 0; i < req.session.cart.length; i++) {
            cartQuantity += req.session.cart[i][1];
        }    
    }
    req.cartQuantity = cartQuantity;
    next();
};

module.exports = getCartQuantity;