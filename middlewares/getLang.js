const getLang = (req, res, next) => {
  if (!req.headers["accept-language"]) {
    req.lang = "undefined";
    next();
  }
  req.lang = req.headers["accept-language"].split(",")[0];
  next();
};

module.exports = getLang;