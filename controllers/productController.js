const product = require("../Models/products");

const productController = {
  getAll: async (req, res) => {
    try {
      const datas = await product.find();
      return datas;
    } catch (error) {
      console.log(error);
      return res.status(500).json(error);
    }
  },
  getById: async (req, res) => {
    try {
      const { id } = req.params;
      const data = await product.findById(id);
      if(!data) {
        return res.status(404).json({ message: "Data not found" });
      }
      return data;
    } catch (error) {
      console.log(error);
      return res.status(500).json(error);
    }
  },
  create: async (req, res) => {
    try {
      console.log(req.body);
      const { productname, price, picture, quantity, description, weight, promotion, date } = req.body;
      const newProduct = new product({
        productname,
        price,
        picture,
        quantity,
        description,
        weight,
        promotion,
        date
      });
      await newProduct.save();
      return res.status(201).redirect("/admin");
    } catch (error) {
      console.log(error);
      return res.status(400).json(error);
    }
  },
  update: async (req, res) => {
    try {
      const { id } = req.params;
      const { productname, price, picture, quantity, description, weight, promotion, date } = req.body;
      const updateProduct = await product.findByIdAndUpdate(id, {
        productname,
        price,
        picture,
        quantity,
        description,
        weight,
        promotion,
        date
      });
      if (!updateProduct) {
        return res.status(404).json({ message: "Data not found" });
      }
      return res.status(200).json(updateProduct);
    } catch (error) {
      console.log(error);
      return res.status(400).json(error);
    }
  },
  delete: async (req, res) => {
    try {
      const { id } = req.params;
      const productDel = await product.findByIdAndDelete(id);
      if (!productDel) {
        return res.status(404).json({ message: "Data not found" });
      }
      return res.status(204).send("Delete Success");
    } catch (error) {
      console.log(error);
      return res.status(400).json(error);
    }
  },

};

module.exports = productController;