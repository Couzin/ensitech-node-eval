const User = require('../Models/users');
const bcrypt = require('bcrypt');

const userController = {
  login: async (req, res) => {
      try {
      // je récupère les données du form (email, password)
      const { email, password } = req.body;
      // je récupère l'utilisateur qui a l'email du form
      const user = await User.findOne({ email });
      // si je ne trouve pas l'utilisateur, je renvoie une erreur
      if(!user){ 
        return res.status(404).json({ message: "Data not found" });
      }
      // je compare le mot de passe du form avec le mot de passe de l'utilisateur
      const validPassword = bcrypt.compareSync(password, user.password); // renvoie true ou false
      // si le mot de passe est bon, je connecte l'utilisateur et je le stocke en session
      //if(validPassword = true) === if(validPassword) === if(true) car validPassword=true
      if(!req.session.user) {
        req.session.user = {};
      }
      const { id, username, access } = user;
      req.session.user = { id, username, access};
      console.log(req.session.user);
      if(validPassword){ 
        return res.status(200).redirect("/products");
      }
    } catch (error) {
      console.log(error);
      return res.status(500).json(error);
    }

  },
  getAll: async (req, res) => {
    try {
      const users = await User.find();
      return res.status(200).json(users);
    } catch (error) {
      console.log(error);
      return res.status(500).json(error);
    }
  },
  getById: async (req, res) => {
    try {
      const { id } = req.params;
      const user = await User.findById(id);
      return res.status(200).json(user);
    } catch (error) {
      console.log(error);
      return res.status(500).json(error);
    }

  },
  create: async (req, res) => {
    try {
      // je récupère les données du form (username, email, password, age, address)
      const { username, email, password, access, birthdate, address } = req.body;
      // je hashe le mot de passe pour le rendre illisible
      const hash = bcrypt.hashSync(password, 10);
      // je créée un nouvel utilisateur a partir du modele
      const newUser = new User({
        username,
        email,
        password: hash,
        access,
        birthdate,
        address,
      })
      // je le sauvegarde en base de données
      await newUser.save();
      // je renvoie une réponse au client
      res.status(201).redirect('/login');
    } catch (error) {
      console.log(error);
      return res.status(500).json(error);
    }
  },
  update: async (req, res) => {
    try {
      const { id } = req.params;
      const { usernname, email, password, age, address } = req.body;
      const updateUser = await usern.findByIdAndUpdate(id, {
        usernname,
        email,
        password,
        age,
        address,
      });
      if (!updateUser) {
        return res.status(404).json({ message: "Data not found" });
      }
      return res.status(200).json(updateUser);
    } catch (error) {
      console.log(error);
      return res.status(400).json(error);
    }
  },
  delete: async (req, res) => {
    try {
      const { id } = req.params;
      await User.findByIdAndDelete(id);
      res.status(204).redirect('/users');
    } catch (error) {
      console.log(error);
      return res.status(500).json(error);
    }
  },
};

module.exports = userController;