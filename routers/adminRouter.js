const express = require('express');
const adminRouter = express.Router();

adminRouter.get('/', (req, res) => {
  res.render("admin", {"cartQuantity": req.cartQuantity, "user": req.session?.user, "lang": req.lang});
});

module.exports = adminRouter;