const express = require('express');
const product = require('../Models/products');
const userRouter = express.Router();
const userController = require('../controllers/userController');
const isLogged = require('../middlewares/isLogged');
const isAdmin = require('../middlewares/isAdmin');
const is18 = require('../middlewares/is18');


//------- user cart --------- //

userRouter.get('/cart', async (req, res) => {
    try {
        const datas = [];

        if (!req.session.cart) {
            return res.render("cart", {datas, "cartQuantity": req.cartQuantity, "user": req.session?.user, "lang": req.lang});
        }

        const products = req.session.cart;

        // for each product in the cart, get the product from the database
        for (let i = 0; i < products.length; i++) {
            // get the product from the database directly, without using the controller
            const data = await product.findById(products[i][0]);
            // initialize an array containing the product and the quantity at the correct position
            datas[i] = [data];
            datas[i][1] = products[i][1];
        }
        return res.render("cart", {datas, "cartQuantity": req.cartQuantity, "user": req.session?.user, "lang": req.lang});
    } catch (error) {
        console.log(error);
        return res.status(500).json(error);
    }

});

// I need to use a get to add a product to the cart because of the html
userRouter.get('/cart/add/:id', (req, res) => {
    const {id} = req.params;
    if (!req.session.cart) {
        req.session.cart = [];
    }
    // if the product is already in the cart
    if (req.session.cart.some((element) => element[0] === id)) {
        // add 1 to the quantity on the correct position
        const index = req.session.cart.findIndex((element) => element[0] === id);
        req.session.cart[index][1] += 1;
        return res.status(200).redirect('/products');
    }
    req.session.cart.push([id, 1]);
    res.status(200).redirect('/products');
});

// ------- Users --------- //

// get all users
userRouter.get('/', isLogged, isAdmin, async (req, res) => {
    const users = await userController.getAll(req, res);
    res.status(200).json(users);
    // res.render("users", {users});
});

// get a user by id
userRouter.get('/:id', async (req, res) => {
    const user = await userController.getById(req, res);
    res.status(200).json(user);
    // res.render("user", {user});
});

// create a new user
userRouter.post('/', is18, async (req, res) => {
    // TODO: Validate body with package
    const newUser = await userController.create(req, res);
    console.log(newUser);
    //res.render("user", {newUser});
});

// update a user
userRouter.patch('/:id', isLogged, async (req, res) => {
    const user = await userController.update(req, res);
    // res.render("user", {user});
});

// delete a user
userRouter.delete('/:id', async (req, res) => {
    const user = await userController.delete(req, res);
    // res.render("user", {user});
});

module.exports = userRouter;