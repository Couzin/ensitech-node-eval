const express = require('express');
const userRouter = require('./userRouter');
const productRouter = require('./productRouter');
const adminRouter = require('./adminRouter');
const userController = require('../controllers/userController');
const getCartQuantity = require('../middlewares/getCartQuantity');
const getLang = require('../middlewares/getLang');

const router = express.Router();

// un router est utilisé pour gérer les routes (endpoints)
// la route appelle ensuite un controller qui va gérer la logique de l'endpoint

// ici on gère la route / avec la méthode GET qui va appeler le controller
router.get('/', getLang, getCartQuantity, (req, res) => {
    console.log(req.headers['accept-language'])
    res.render("home", {"cartQuantity": req.cartQuantity, "user": req.session?.user, "lang": req.lang});
});

router.get('/login', getLang, getCartQuantity, (req, res) => {
    res.render("login", {"cartQuantity": req.cartQuantity, "user": req.session?.user, "lang": req.lang});
});

router.post('/login', getLang, getCartQuantity, async (req, res) => {
    console.log(req.body);
    await userController.login(req, res);
});

router.get('/register', getLang, getCartQuantity, (req, res) => {
    res.render("register", {"cartQuantity": req.cartQuantity, "user": req.session?.user, "lang": req.lang});
});

router.get('/illegal', getLang, getCartQuantity, (req, res) => {
    res.render("illegal", {"cartQuantity": req.cartQuantity, "user": req.session?.user, "lang": req.lang});
});

router.get('/logout', getLang, getCartQuantity, (req, res) => {
    req.session.destroy();
    res.render("home", {"cartQuantity": req.cartQuantity, "user": req.session?.user, "lang": req.lang});
});

router.use('/admin', getLang, getCartQuantity, adminRouter);

router.use('/users', getLang, getCartQuantity, userRouter);

router.use('/products', getLang, getCartQuantity, productRouter);

// si aucune route n'est trouvée, on affiche une page 404
router.get('*', getLang, getCartQuantity, (req, res) => {
    res.render('404', {"cartQuantity": req.cartQuantity, "user": req.session?.user, "lang": req.lang});
});

module.exports = router;