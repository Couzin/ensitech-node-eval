const express = require('express');
const controller = require('../controllers/productController');
const isAdmin = require('../middlewares/isAdmin');

const productRouter = express.Router();

productRouter.get('/', async (req, res) => {
    const datas = await controller.getAll(req, res);
    res.render("products", {datas, "cartQuantity": req.cartQuantity, "user": req.session?.user, "lang": req.lang});
});

productRouter.get('/:id', async (req, res) => {
    const {id} = req.params;
    if (!id) {
        return res.status(400).json({message: "Id is required"});
    }
    const data = await controller.getById(req, res);
    res.render("product", {"product": data, "cartQuantity": req.cartQuantity, "user": req.session?.user, "lang": req.lang});
});

productRouter.post('/', isAdmin, async (req, res) => {
    // TODO: Validate body with package
    const newProduct = await controller.create(req, res);
    console.log(newProduct);
    //res.render("product", {newProduct});
});

productRouter.patch('/:id', async (req, res) => {
    // TODO: Validate body with package
    const {id} = req.params;
    if (!id) {
        return res.status(400).json({message: "Id is required"});
    }
    await controller.update(req, res);
    res.status(200);
});

productRouter.delete('/:id', async (req, res) => {
    const {id} = req.params;
    if (!id) {
        return res.status(400).json({message: "Id is required"});
    }
    await controller.delete(req, res);
    res.status(204)
});


module.exports = productRouter;