require('dotenv').config();
const express = require('express');
const router = require('./routers/router');
const path = require('path');
const app = express();
const mongoose  = require('mongoose');
const session   = require('express-session');

mongoose.connect(process.env.MONGO_URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true
})

// creation de la session
app.use(session({
    secret: process.env.SESSION_SECRET,
    resave: false,
    saveUninitialized: false,
    cookie: { secure: false }
}))

// encode les données du formulaire
app.use(express.urlencoded({ extended: false }));
// format json
app.use(express.json());

// dire a node que l'on utilise ejs comme moteur de template
app.set('view engine', 'ejs');
// definir le dossier des vues
app.set('views', path.join(__dirname, 'views'));
// déclarer le dossier des statiques
app.use(express.static(path.join(__dirname, 'public')));

// dire au serveur d'utiliser le router
app.use(router);

// lance le serveur sur le port 3000
app.listen(process.env.PORT, () => {
    console.log('Server is running on port 3000');
});
