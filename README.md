# Evaluation Nodejs

## Consignes

1. Création d'un CRUD user ( password : hash + salt )
2. Système de connexion (session)
3. Création d'un CRUD product
4. gestion des permissions
5. interface qui affiche les produits, avec un message "X produit disponible"
6. Admin -> bouton ajouter un produit qui redirige vers un form uniquement pour les admins
7. Quand je clique sur un produit, je suis redirigé vers "/product/:id"
8. mettre en forme le site

Bonus 
- systeme d'ajout de panier (session)
- Afficher la langue de l'utilisateur dans le footer
- Mettre les prix en promo

## Installation

- clone repo
- `npm install`
- add .env file with the following variables
  - PORT
  - MONGO_URI
  - SESSION_SECRET
- npm run dev

## Improvments

- Better error handling
- Data validation
- sanitize data
- use js scripts for frontend and improve ejs usage
- css improvements
- clear cart, remove, and arrows in cart page needs to be implemented
- images size
- disociate cart and user
- refacto
  
## Tips 

### Asynchrone

Cette fonction interrompt l'execution du code jusqu'à ce que la fonction qui prend du temps soit terminée.

```javascript
const synchrone = () => {
    console.log('synchrone');
}
```
Pour éviter de bloquer l'execution du code, on utilise le mot clé `async` devant la fonction. Qui permet de dire au programme de faire autre chose et de reprendre quand `await` est fini.

```javascript
const asynchrone = async () => {
    const response = await fonctionQuiPrendDuTemps();
    return response;
};
```
### Git

Pour créer une branche : `git checkout -b nomDeLaBranche`  
Pour ajouter le code à git avant de faire un commit : `git add .`  
Pour faire un commit : `git commit -m "message"` (faire un message explicite)  
Pour envoyer du code sur le repo distant : `git push `  

Pour récupérer du code et le fusionner avec le code local : `git fetch` (récupère) puis `git rebase origin/nom-de-la-branche` (fusionne)  
