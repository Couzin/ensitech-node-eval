const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const productSchema = new Schema({
    productname: { type: String, required: true },
    price: { type: String, required: true },
    picture: { type: String, required: true },
    quantity: { type: Number, required: true },
    description: { type: String, required: true },
    weight: { type: String, default: "2" },
    promotion: { type: String },
    date: { type: Date, required: true, default: Date.now },
}) 

let products = mongoose.model("products", productSchema);

module.exports = products;